BootStrap: localimage
From: apptainer/sif-img-files/micromamba.sif
Stage: build

%arguments
    ENV_NAME=hdf5-kernel-env
    CND_FILE=hdf5-kernel-env.yml

%files
    # Copy conda environment yaml file from host to image
    {{ CND_FILE }}

%post
    # Create conda environment from yaml file
    ENVDIR=/opt/conda/envs/{{ ENV_NAME }}
    /usr/bin/micromamba env create --yes --quiet \
        --file {{ CND_FILE }} \
        --prefix $ENVDIR

    # Clean up
    /usr/bin/micromamba clean --all --yes
    find /opt/conda -follow -type f -name '*.a' -delete
    find /opt/conda -follow -type f -name '*.pyc' -delete
    find /opt/conda -follow -type f -name '*.js.map' -delete
    find /opt/conda -follow -type d -name '__pycache__' -delete

BootStrap: localimage
From: apptainer/sif-img-files/micromamba.sif
Stage: final

%arguments
    ENV_NAME=hdf5-kernel-env

%files from build
    # Only the conda environment is needed
    /opt/conda/envs

%environment
    # Set environmental variables
    export LC_ALL=C

    # PATH and PYTHONPATH
    export ENVDIR=/opt/conda/envs/{{ ENV_NAME }}
    export PATH=$ENVDIR/bin:$PATH
    python_lib_dir=$(find $ENVDIR/lib/ \
        -maxdepth 1 ! -type l -type d -name 'python*' | head -1)
    export PYTHONPATH=$python_lib_dir:$PYTHONPATH

    # LD_LIBRARY_PATH
    export LD_LIBRARY_PATH=$ENVDIR/lib:$LD_LIBRARY_PATH

%runscript
    exec "$@"

%labels
    Author Jason Brudvik
    Version 2024-12-02
