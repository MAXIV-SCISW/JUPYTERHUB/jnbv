# JNBV CHANGELOG

## 2024.12.10 (2024-12-10)

*  added cuda libraies to jnbv image, updated readme [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/4258c25935f3da32df988636c9a726e63f452f64)
*  removed cuda libraries from jnbv image, pinned jnbv version in image [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/2728b41b51e73e4334a6e02efe0fa8bd19b0596d)
*  small changes to base images [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/a0bccdf07af865a47bd40e4d8e30225e4a2c338a)
*  slight change to default output folder name and permissions [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/a859d7de025b7b346c5452fb720dc750210965d9)


## 2024.11.27 (2024-11-27)

*  updated readme - suggest iantll via pip instead of conda-forge [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/600883159bff24aaa0acc5084077ebe921e3ed20)
*  testing use of apptainer image for running jnbv [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/ec7bbcf441368e1fdcc36470b8309cacf15e3053)
*  alter CI trigger - only for tags [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/e8e479f4a7f5bdaf5c884d6c736a239141b01ce3)
*  jnbv apptainer image working, example hdf5 kernel image working [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/8c9c9cdfc55db95dd723742732da856885671f2b)
*  updated readme [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/a2f6b1ac16ebbc8f28df478443f50aa47e4e58f3)
*  added fuse libraries to jnbv sif image [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/0de56aa3f0e8ebb9407ac01ec3bb230d38604de4)
*  updated readme [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/8b267ae66d20e0bb280993bbd9e7664b49e494b1)
*  added save directory option [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/7d60459e54adb5e08d957a68cf5f593d4dafdf13)
*  bumped version in pyproject.toml to 2024.11.27 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/af9c9ca7222bc363c7a117fba8cd7f57d06d11fc)


## 2024.11.19 (2024-11-19)

*  trying to figure out why azure pipeline fails at docker image build [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/78a215864612bf4c852af3aaf33b6b33f1324745)
*  try building without setuptools [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/5ad9fb97d82a5aedb2574e2745553adc696e3123)
*  added to pyproject build-system requirements [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/b4cb6c23805800a21475f788fa6209e8473ba0f2)
*  slight change to gitlab ci stages [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/efb0e35e7303d707ccbec2a28ca8fe1c86c1a8db)
*  bumped version in pyproject.toml to 2024.11.19 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/e04ef094b440847ab33965592e44173cd95727b9)


## 2024.11.18 (2024-11-18)

*  updated to use of pyproject.toml instead of setup.cfg [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/1374cce82739c56e843caa423188df9a20c59166)
*  bumped version in pyproject.toml to 2024.11.18 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/bb35f81ae20ecd05d4626433283ae4b839f50d1f)
*  updated build commands [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/e18c6abfc1fee8a144036c0e265eab7b250feba1)


## 2024.11.15 (2024-11-15)

*  changed image used in gitlab ci, changed from conda to mamba, updated tox tests used [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/55c503c3fbada62066bf709a74100d69e6238d00)
*  added toc-conda install to gitlab ci [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/724fd8dfa357f1bc999015c8923c2509a0db332a)
*  missing package py [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/d6c51a7fc6fae629dfc02f87266fa5097dda6f9b)
*  problem with newer versions of tox? [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/7d64df475945dec472749c16a91fa6741a8de3e7)
*  newer versions of tox seem to have some issues, limiting to <4.0 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/8ebee87383ce710d2218133ee3b5a2eb6fff09e8)
*  updated execution of a few notebooks [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/d6a20a269feb2e36a5c93f7031b138e701c3c732)
*  removed unnecessary lines [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/fb6231d7a1420edddb9e7acb1f80b1644a894369)
*  bumped version in setup.cfg to 2024.11.15 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/28efe87a519eb7eb3ab115a4fcf1ca4771a296b7)


## 2024.11.14 (2024-11-14)

*  changed makefile target name [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/e5822ba0e20eb3352af9868fff05041c244c95f2)
*  able to get dev environment working, lint test passes [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/06c4b70bc23f2b681a33743437f7565ff8ff5917)
*  working on getting tox tests working [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/1b626a233a7fa9cc70fc8b92b0761fa5dfeced9f)
*  working using pytest for validations, apptainer kernels work fine [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/1e820bbd25deedce16afa2b75ee340abaff1ae62)
*  removed attempter pytest integration with core of jnbv - too cumbersome [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/22070e293a235aa10a17513684cfe22e0bd9a280)
*  for build set python version range to match papermill, updated test environments [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/46647776826e9e74efb0905f9085334c9a645cf1)
*  bumped version in setup.cfg to 2024.11.14 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/85af4e00b73f94e9345fbdc3dce1b171fdea1524)


## 2022.1.29 (2022-01-29)

*  changelog now reverse chronological order [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/299982ab5cfb2e9953ddf51d4a0d40714e9801fe)
*  added warning to ignore [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/69aa56934e46bf356fdff9962d54898484f2393b)
*  bumped version in setup.cfg to 2022.1.29 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/e61f6faa4c6df4f6d466fc7df7d416e312855c61)


## 2022.1.26 (2022-01-26)

*  added ignoring of some h5web output [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/606169c404731f5bcb3a18b550abbb5b0981c63b)
*  bumped version in setup.cfg to 2022.1.26 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/50a8ffe963abbed866b6fc04390044b3ebf79570)


## 2021.11.22 (2021-11-22)

*  added to list of things to ignore [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/316a099ec949b3b8bd3cf5e12530770051357907)
*  updated notes on developemt work [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/b62af0a0c63f7dd9b3e4325ba63a4fe9b18c009f)
*  bumped version in setup.cfg to 2021.11.22 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/a04bb6c2c0afdb1fb161310642c0362434383652)


## 2021.11.4 (2021-11-04)

*  added some timing keywords to the list of inputs to ignore [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/ba1efe9e6e5bdb30c5bf14107fa8fc3499e421d6)
*  bumped version in setup.cfg to 2021.11.4 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/9e2cab8b15f0f0f393b93420d50d109b569baaae)
*  bumped version in setup.cfg to 2021.11.4 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/d6353dc459804235c962575fe2bb27e25702b782)


## 2021.10.26 (2021-10-26)

*  added to list of source to ignore [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/10bc8385bb9f28aa64e892fdba7825dd415ef00d)
*  updateed execution of test notebook [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/389a54f5976f888a9b4d80fba7ec6632209d1267)
*  updated the execution of a couple notebooks [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/77841fafef31f05e04a1c293aa702885dba72841)
*  bumped version in setup.cfg to 2021.10.26 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/711acc314c405d988c6dd45f024bfa9c817b8ad6)


## 2021.8.26 (2021-08-26)

*  updated license [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/c7e63c419cdfc582c711876b0a5432fad4f10fce)
*  trying to setup sast [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/1d4ca22ac71331ed2f4116485b86373776e68e50)
*  try adding script to SAST job [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/f77ce57bf922d96d8641f098f8aea54a6d2da952)
*  trying again [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/4495db0f2f0944991e08a138fe74dfd5c552fce6)
*  added artifacts [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/274dce94df27430e7699c9c06083a86cd2c14540)
*  try adding a gl-sast-report.json file [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/72cbc1800673fa29a6b3a011394b6dfa848dae09)
*  try adding gl-sast-report.json [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/41f4022ff7bd2e4d350dc8e1e29a78e173ce5435)
*  trying some other things... [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/8073da453df71ade79de1075aebfc645f9a076e3)
*  trying something else [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/a9e4cdb75684d9d312149cd2e8a6cbd9af50a674)
*  trying appthreat/sast-scan [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/e098c247fd29774dca06b37b5d20201c5a5fda9b)
*  changed before_script for sast job [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/5b101be287e9e096012fe00c7fd81f1f5453d3b0)
*  changed type to python [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/370a73533c45541682a47008b3baa832de9f1d58)
*  changed job name [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/41f7fe9a75130bf36da0b9c519a1b6b997e4392f)
*  trying semgrep [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/8d4d1b93ad971f852305c854bf9d4755fe16aaa8)
*  added berfore_script to semgrep job [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/d9c7ae602d4b6ef46194045df7ca469727da8023)
*  change to semgrap command [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/7ae733a1525dc78c552ef641263c5093a6f1f863)
*  try some other semgrep settings [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/5b8e9eeea158a4960c3f2b63a7311e32738ce35d)
*  other semgrep settings [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/0d6e55520dfa77ee4c782ce530e346b0216b6105)
*  trying to view artifact [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/9fb7dab64fab94081d47d9e9f6d98fbf8e28c72b)
*  trying other semgrep settings [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/451b8bdcca9470486a0dcaaaf0b1953aaa10013b)
*  trying bandit for security testing, replaced assert statements with raise AssertionError [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/bfef7ffd405f65c7d781628ee4aa0b87a85848ec)
*  added pip install [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/de6849a2d1c2cf1d14e5287d6e58bdacb4e60477)
*  adding more to bandit checks [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/cfd4e87013efbafee555d75cc34f19003999ad30)
*  changed asserrt statment to comply with bandit errors [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/861d15f934d47f28be0fa4e16d410672f7b0aa0d)
*  trying different bandit command [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/3a76e47f0cd5a64cb07bfa702f935e35d12fc9d5)
*  change to bandit scan trigger [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/4b7dbbf6bb6b91b952ee980a0881e3f4820bc88d)
*  added creation of bandit reports [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/d934e9445e27594fc4e022c4c0da5c62f5deba3b)
*  bumped version in setup.cfg to 2021.8.26 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/eaf695f0486b0f7903dca044554b19d1ea06239a)


## 2021.8.18 (2021-08-18)

*  updated readme [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/1ba9b35df815af78c70a3fd6559f17c39f7008c2)
*  added a source line to ignore [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/548558e65c5a466e1d6ca23f54403e016c0525c7)
*  added combining of stderr outputs [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/afee97a6b15e1b20e1ea65c0d907635e15a81d51)
*  bumped version in setup.cfg to 2021.8.18 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/aff8a234ab01d4e7a7d536ff08a25a3e4997d75d)


## 2021.8.17.1 (2021-08-17)

*  added retry of ntoebook execution in case of kernel dying [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/72bd622613ad1a42f8c22485bcf3838ee3b61b2a)
*  added comparison test, fixed mistake in execution function [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/33fd9fe6db86f4a0602a5e8156212547697500b0)
*  added special validation notebook for py36 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/24ec028e729e97205c5331512f2cb927f413e360)
*  bumped version in setup.cfg to 2021.8.17.1 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/cf9bcafd95ac266d862138184ae542c97be6aa45)


## 2021.8.17 (2021-08-17)

*  updated screenshot [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/d64817df7278c559695ce3a74df3230edc09b14f)
*  added warning to ignore [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/e4b1b55d564c9d329cf4ce48f0ad5c365bc945ae)
*  bumped version in setup.cfg to 2021.8.17 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/7462ca2f53188c4e9dafe94bd6250da74b652963)
*  bumped version in setup.cfg to 2021.8.17.1 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/57627b7a02eafeb86cf9ce5e7cb9c04d866dbe37)
*  slight change to versioning setup [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/11355e80fbbc146d7de5221693129955f4a3c2ab)
*  fixed typo [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/3381208a5a82669c28073c139e81b72351cad62f)
*  bumped version in setup.cfg to 2021.8.17 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/f25191925662f40586790c72806b76f6223acb1d)


## 2021.8.16.0 (2021-08-16)

*  small changes to comments [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/d6b301bbf6ad92b05507e6a5b1f437a4d167c673)
*  added function for removal of warnings in cell output [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/4b1f175bb23f996e4d2a83a58a331836059ba5cf)
*  small change to output [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/fa8ad427ba3511caf780b21b857469138ddefd1b)
*  removed old comments [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/1515e20819ed47146acf06f908b93d883392f1fb)
*  added a couple tests and dependencies needed in kernel environments [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/18d6b266df6aa302d3be36d88e54c83a4cd78c7b)
*  had forgotten to add removal of warnings from both outputs when doing comparisons [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/89c40aca1e7a53baddc4262d74691e88db876ea1)
*  added another wanring to ignore, added check for existance of subkey [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/8cfadd227ded72692ac17be2fdcd39442b1e5dd9)
*  bumped version in setup.cfg to 2021.8.16.0 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/4de6e8319946c932bfbc432dace05b8ff9d8dd49)


## 2021.8.4.0 (2021-08-04)

*  reorganization of tests in tox configuration [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/b91683118fc657ae32aaff0a645a6c9b47412f6d)
*  small edits of documentation [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/fb24be93e3a7e95620654384afcb344097d146a9)
*  bumped version in setup.cfg to 2021.8.4.0 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/47cd2098ba657659c4cd1f3594bc21a5022e0e5e)


## 2021.8.3.0 (2021-08-03)

*  added makefile targets for new tests, added badges to top of readme [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/9ece8d3114f5168940c9ee43fe15659f6a054100)
*  updated documentation a bit [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/f8577562775c8ffaad13ffa11e8d42bf266b1670)
*  edited screenshots slightly [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/d1983713e3f063c7ffafff4765e777ee24a2d0d9)
*  added tests for reading of notebooks [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/9a7713666de4aa941992839bd3110d4564bbe99d)
*  try to make reading tests fail [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/8527155c0deb942d0531313ebd144dc461f4b90c)
*  change to exection CI test triggers [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/a319f05c08c55af246d0db616c3ba12d349ddc88)
*  added tests of test_ipynb function [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/637531e54031897428b680d0489aa36000a2375b)
*  fixed incorrect asserion tests [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/523c8f2925b04aafef50c9ade6e65fe689dadb36)
*  added a bit to the documentation [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/560ccd65f6df803848594aedcb72c43921a64ee4)
*  added simple changelog generator target to makefile [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/3f8cc1236547e9fbe9b5bde871739e28ddf006ef)
*  added comparison tests, combined some test executions in tox config [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/19cb4f58c4c67334bad4c444d300bcaa30d54cb9)
*  fixed typo, changed some CI job names [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/24381eac34e5bc849e83e47ac643325d8a35d822)
*  combined some tests in tox config [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/565fcedf5350c1f93c43b4837540b4e8e83abf09)
*  changed test function names [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/809f831afad0f94a3dc3f17688ef8ee19a244ccf)
*  changed some test names [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/a1e393774609235dabefdff25b12016d477c63b9)
*  fixed incorrect test name in CI config [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/99033b5cf53cdf9427385c9cad797d6ee7a0360d)
*  slight comment change [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/47e48dd1542ed87c6d5288ee8c428731ccb111ab)
*  added some comments [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/f7023fe43437804ccabe7a5a4ecdc8dfbced573f)
*  small change to comparison code, added a test [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/558292fead5aea7ab721a461b7aad3ae8e2b56f4)
*  simplified validation tests, made comparison check for newline better [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/c378cf6822e0edd3393fde8e191ceacb9befdb85)
*  added some more execution and validation tests [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/34d9c60b2f9e5fc3d9fa01875997239cc5158bba)
*  trying to get varsioning, tagging, and changelog to all work nice together [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/80e8fe3fcf99419364bbca2cd52aa8b055490c9f)
*  bumped version in setup.cfg to 2021.8.3.0 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/d8bd5c4b9fe9f5b0d94d35fba131c445b48b7dc8)
*  bumped version in setup.cfg to 2021.8.3.0 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/237819141b60366ac2c8fce02f047e63b2c8f470)


## 2021.7.28.0 (2021-07-28)

*  cropped screenshots [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/8e0e553bbbcce461d4bc2d171faa31dea45981e5)
*  created pytest for execution [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/5107eacec59b01ef8aa623ce39a65f1036fc0a4e)
*  fixed mistake in tox test, added to documentation [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/eaf1bdf5d388c8596c01c726d5475b5cffa38b9a)
*  fixed some documentation mistakes [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/24188f986e970316d111fdff159fc9c6b1d1d838)
*  small edits of documentation [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/9ae1f1f6daf69fd165b51befec4e8607d7f9c48b)
*  change to CI triggers, slimmed test_execute_success_hdf5_kernel() [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/ec588a0dff991ea730e868d940559dda1e783e22)
*  added various tests of notebook execution [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/84d434846e087ee60c142339bdb859baa14da94b)
*  some changes to execution tests [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/cda80cba580792842990d8f913f6e8faef87798a)
*  fixed incorrect names [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/d97f631b1d24ccf9fc787d9120a7da06edc9b0b2)
*  changed test names, making CI more compact [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/6d5505ea89e5f348b80cb194c3cfb3cd72dbdefa)
*  bumped version in setup.cfg to 2021.7.28.0 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/07ee9c01c61b15a89bb2b5bf4e1dd6c4951235b8)


## 2021.7.21.3 (2021-07-21)

*  updated documentation [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/82b9cea6798a170f528df37247a2ea23b3a8bc9a)
*  updated documentation [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/bc54f51dd5bf0f03a753f96648c65c0756200c66)
*  bumped version in setup.cfg to 2021.7.21.3 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/e7f8d9a992de846c2ac566b82f8e34e29c3798b8)


## 2021.7.21.2 (2021-07-21)

*  created makefile target to upload to PyPi, altered CI to upload to PyPi [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/5a8ed2df86e407be5b945a5624c28d0e8f496446)
*  bumped version in setup.cfg to 2021.7.21.2 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/89202b0b895309f4555e5b7e076234d3ea2214cb)


## 2021.7.21.1 (2021-07-21)

*  bumped version in setup.cfg to 2021.7.21.1 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/5846e79bc2a390369f19554761aedcc950e7a5c8)


## 2021.7.21.0 (2021-07-21)

*  have commit counts start at 0 instead of 1 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/6401cb3ec47bb9366b7225b1abf05f5e0d568213)
*  bumped version in setup.cfg to 2021.7.21.0 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/c62d6739562f1bd6557cff00d8d97f9a0ce175a3)


## 2021.7.19.9 (2021-07-19)

*  made link to CONTRIBUTING.md accessible from pypi page [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/c7ced77ba139a4e33a4012c51ac0568dface8d04)
*  bumped version in setup.cfg to 2021.7.19 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/8fe577763e4006f8c0c81f175011f200c66f4da9)
*  bumped version in setup.cfg to 2021.7.19_8fe57776 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/7976e90264038b6268e66c22654f24e2daadb7d3)
*  bumped version in setup.cfg to 2021.7.19.7976e902 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/6ccdf2a1d2688f0b7d8e29fb22595761511d5a47)
*  bumped version in setup.cfg to 2021.7.19.dev6ccdf2a1 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/1cac8fe4416859f3ea7d00f410756d906d9f0445)
*  bumped version in setup.cfg to 2021.7.19_5 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/a6f4613b729bbb1e4f7c80309216f4396885cb08)
*  bumped version in setup.cfg to 2021.7.19-6 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/7e6c5640d5137f5b5d9f683992707c08c9c3db15)
*  bumped version in setup.cfg to 2021.7.19.7 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/d1bf8a737c8de54efe2abf402b9efa0dd7da0c88)
*  bumped version in setup.cfg to 2021.7.19.8 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/788772057e0af1fa6af33b6b13b7dd9e4d0100a1)
*  updated documentation [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/1d6f44a7d6c38a46aed1ccc7e302c2cb9b509b67)
*  bumped version in setup.cfg to 2021.7.19.9 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/f2985ff74d270528d01626486b8cd2af0e444da6)


## 2021.7.19 (2021-07-19)

*  updated readme, added comments to yaml files [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/71f5950c27049fe15ce791aad805914401bf7d87)
*  fixed typos [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/64a186e89717cd13e6fae3f7a46029a5ed7dbdd2)
*  changed triggers for some CI stages [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/c34531ec4fb34ae7d5cff8f8bc41f42af2b90afd)
*  making png image pypi friendly [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/f25cbd5a3da91729094fef79774d279ebb8c5791)
*  bumped version in setup.cfg to 2021.7.19 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/c2c6bd7660e29dd726efb1d97f1d22faaec4e510)


## 2021.7.16 (2021-07-16)

*  re-grouping of makefile targets, new screenshot [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/256bde347353b82c19f9f7f2afa5ab1fe38efe54)
*  updated documentation [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/18b815ca82412c77aa3130cbeebd2ea5ee0dd8d2)
*  small change to target and stage name [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/01341423c7da6df7b5e5c7222ab68efc11c94ae0)
*  added a bit to the documentation [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/3d2c851df4cedc96de9e993777fdf0614c56c992)
*  bumped version in setup.cfg to 2021.7.16 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/6910c35c6e8c4e32fb3446bdf0d284dcf777f6e8)


## 2021.7.15 (2021-07-15)

*  small comment edit [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/7c48bc75c88dbe42149531b488d7ad59c758e4d6)
*  created test of jnbv terminal execution [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/476c61b6475ec28100ca362bccfca88a37f366cf)
*  added new jnbv temrinal execution test to CI [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/a69379a32276ad70ef56a4ff64062e13ee34b4d6)
*  try unpinning version in env yaml [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/a17186a5414f547413d30de6d6a45cc3e50ad79b)
*  remove basepython configuration [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/9900bc7a49cc888cd83f99675c0cbdd179d772cd)
*  removed some modules, try to figure out issue [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/9250b1e1184d7884bcafe023d5d5c66b35ee16ce)
*  try adding back in validation test [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/6f09e1830bae45e5f9e0f2f254a67b81bef742fa)
*  ipykenrel is needed, but seems to be the source of issue when installing in gitlab CI [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/7320c524515a5ed845f0c878c7188f3d2379c5b6)
*  try adding validation test again [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/6d2ef46d4c5952948a20f2bb83b72b04e345efc1)
*  cleaned up some comments, changed makefile target names [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/f708db96fc5eb6df29876bc2666ecf3880f6f8db)
*  created a validation test for use with pytest [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/360ccff78d12bb7a7e1aca69114ff8c69012b26e)
*  pytest excution working with all versions of python [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/cd368b812a4ef740f24324e7d4620671880d9efa)
*  fixed incorrect test env name [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/37c9ffbdb47154cf88d0cd97b4bf19ac02c2d76c)
*  split testing environments - one for basic building, the other for testing of a kernel [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/afce27f2e261ee69e07de8330f095b472561aae3)
*  updated instructions and screenshot [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/06cfc589ab14de8d23d2f10d47beb3c77402c80d)
*  bumped version in setup.cfg to 2021.7.15 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/45083023626c45b7cef8a88ecd8045e03b4d7666)


## 2021.7.12 (2021-07-12)

*  some cleaning up of nested if-statements, added example hdf5 kernel, notebook, data [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/b5b5956490e37054151a0ccd747a7c7708c60732)
*  some more cleaning up [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/ad82ff4108b3af23174014c9760741e7c633a3fb)
*  max-complexity in flake8 now 10 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/85f0e368293bd2f2aee7b1cf77f56e52b2cc2fb4)
*  a bit more if-nesting removal [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/b37788cd2d3a88a86959251deaf844858c1e2345)
*  added sourcing of conda env prior to test execution [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/e6b46ad4ba8f919be9dca0adb40fd36a9cc6f75e)
*  bumped version in setup.cfg to 2021.7.12 [View](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv/-/commit/a84dab907aa7fc0cec5fe37fc8440cd0b2d6da92)


